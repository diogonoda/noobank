class AuthenticateAccount
  prepend SimpleCommand
  attr_accessor :email, :password

  def initialize(email, password)
    @email = email
    @password = password
  end
  
  def call
    JsonWebToken.encode(account_id: account.id) if account
  end

  private
    def account
      account = Account.find_by_email(email)
      return account if account && account.authenticate(password)

      errors.add :account_authentication, 'Invalid credentials'
      nil
    end
end
