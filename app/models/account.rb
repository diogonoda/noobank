class Account < ApplicationRecord
  class UnavailableBalanceError < Exception; end;
  class InvalidAmountError < Exception; end;
  class InexistentRecipientError < Exception; end;

  has_many :source_transactions, class_name: 'Transaction', foreign_key: 'source_account_id'
  has_many :destiny_transactions, class_name: 'Transaction', foreign_key: 'destiny_account_id'

  validates_presence_of :customer_name, :email, :password_digest
  validates :email, uniqueness: true

  has_secure_password

  def balance
    current_balance = incomes - outcomes
  end

  def incomes
    destiny_transactions.sum(:amount)
  end

  def outcomes
    source_transactions.sum(:amount)
  end

  def amount_available?(amount)
    balance - amount >= 0
  end

  def create_transaction(destiny_account_id, amount)
    raise InvalidAmountError, 'Amount must be greater then zero' unless amount >= 0
    raise UnavailableBalanceError, 'Insufficient funds' unless amount_available?(amount)
    raise InexistentRecipientError, 'Recipient account not found' unless Account.exists?(destiny_account_id)

    Transaction.create!(source_account_id: self.id,
                        destiny_account_id: destiny_account_id,
                        amount: amount)
  end
end
