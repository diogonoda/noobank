class Transaction < ApplicationRecord
  belongs_to :destiny_account, class_name: 'Account'
  belongs_to :source_account, class_name: 'Account'
end
