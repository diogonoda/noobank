class V1::AccountsController < ApplicationController
  skip_before_action :authenticate_request, only: %i[login register]

  include ActionView::Helpers::NumberHelper

  def register
    @account = Account.create(account_params)

    if @account.save
      response = { message: 'Account created successfully' }
      render json: response, status: :created
    else
      render json: @account.errors, status: :bad
    end
  end

  def login
    authenticate params[:email], params[:password]
  end

  def current_balance
    formatted_balance = number_to_currency(@current_account.balance, unit: 'R$ ', separator: ',', delimiter: '.')

    render json: { :current_balance => formatted_balance }, status: :ok
  end

  def transfer_amount
    @current_account.create_transaction(params[:destiny_account].to_i, params[:amount].to_d)

    render status: :ok
  rescue Account::UnavailableBalanceError, Account::InvalidAmountError, Account::InexistentRecipientError => e
    render json: { message: e.message }, status: :expectation_failed
  end

  private
    def account_params
      params.permit(:id, :customer_name, :email, :password)
    end

    def authenticate(email, password)
      command = AuthenticateAccount.call(email, password)

      if command.success?
        render json: {
          access_token: command.result,
          message: 'Login Successful'
        }
      else
        render json: { error: command.errors }, status: :unauthorized
      end
    end
end
