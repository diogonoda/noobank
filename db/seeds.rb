# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

source_account = Account.create(customer_name: 'Pai da Bettina', email: 'pai@bettina.com', password: '123456')
destiny_account = Account.create(customer_name: 'Bettina Rudolph', email: 'bettina@rudolph.com', password: '123456')

Transaction.create(source_account: source_account, destiny_account: destiny_account, amount: 1000.00)

investidor_anjo = Account.create(customer_name: 'Investidor Anjo', email: 'investidor@anjo.com', password: '123456')

Transaction.create(destiny_account: investidor_anjo, amount: 2000000)
