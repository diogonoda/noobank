class ChangeColumnNullable < ActiveRecord::Migration[5.2]
  def change
    change_column_null(:transactions, :source_account_id, true)
  end
end
