class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.references :source_account, index: true, null: false
      t.references :destiny_account, index: true, null: false
      t.decimal :amount, precision: 38, scale: 2

      t.timestamps
    end
  end
end
