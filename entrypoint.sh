#!/bin/bash
set -e

rm -f /app/noobank/tmp/pids/server.pid

exec "$@"
