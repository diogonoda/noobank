Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post 'auth/register', to: 'v1/accounts#register'
  post 'auth/login', to: 'v1/accounts#login'

  get 'v1/account/balance', to: 'v1/accounts#current_balance'
  post 'v1/account/transfer', to: 'v1/accounts#transfer_amount'
end
