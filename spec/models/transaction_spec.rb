require 'rails_helper'

RSpec.describe Transaction, type: :model do
  it { should belong_to(:destiny_account) }
  it { should belong_to(:source_account) }
end
