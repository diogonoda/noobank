require 'rails_helper'

RSpec.describe Account, type: :model do
  it { should have_many(:source_transactions) }
  it { should have_many(:destiny_transactions) }

  it { should validate_presence_of(:customer_name) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password_digest) }
end
