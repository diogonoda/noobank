# NooBank

* Getting Started

Use Docker and be happy!!!

```docker-compose build```

```docker-compose run web rails db:migrate```


```docker-compose up```

Maybe you have to become owner of some files:

```chown -R $USER:$USER .```



No Docker? Ok, take a look at System Dependencies and Configuration, and then come back here!

Create database

```rails db:migrate```

Then simply

```rails -s```

* API's

##### Create your account on NooBank:

POST /auth/register

{ customer_name: "Your Name", email: "your@email.com", password: "yourPassword"}


##### Get your authentication token:

POST /auth/login

{ email: "your@email.com", password: "yourPassword" }


##### Check you account balance

GET v1/account/balance

Authorization Token on Header


##### Transfer money to other accounts

POST v1/account/transfer

{ destiny_account: 6, amount: 11 }

* System dependencies

Postgresql


* Configuration

Setup database at config/database.yml.


* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Contributing

We are looking foward to get your pull request!!!

Steps

1. Fork the repo
2. Run the tests
3. Make your changes
4. Follow the rules --> https://github.com/magnetis/styleguide/blob/master/Ruby.md
5. Test your changes
6. Create a Pull Request
